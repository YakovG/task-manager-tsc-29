package ru.goloshchapov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.Domain;
import ru.goloshchapov.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataJsonLoadFasterXmlCommand extends AbstractDataCommand{

    @NotNull
    public static final String NAME = "data-json-fasterxml-load";

    @NotNull public static final String DESCRIPTION = "Load data from JSON (FASTERXML)";

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return NAME;
    }

    @Override
    public @NotNull String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON LOAD]");
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
