package ru.goloshchapov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class LoginCommand extends AbstractCommand {

    @NotNull public static final String NAME = "login";

    @NotNull public static final String DESCRIPTION = "Login to program";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("[ENTER LOGIN]");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD]");
        @Nullable final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login,password);
    }
}
