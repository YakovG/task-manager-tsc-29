package ru.goloshchapov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.api.repository.ICommandRepository;
import ru.goloshchapov.tm.api.service.ICommandService;
import ru.goloshchapov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.List;

public final class CommandService implements ICommandService {

    @NotNull private final ICommandRepository commandRepository;

    @NotNull
    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommandList() {
        return commandRepository.getCommandList();
    }

    @NotNull
    @Override
    public Collection<String> getListCommandNames() {
        return commandRepository.getCommandNames();
    }

    @NotNull
    @Override
    public Collection<String> getListCommandArgs() {
        return commandRepository.getArgumentNames();
    }

}
